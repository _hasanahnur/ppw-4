from django.shortcuts import render

def index(request):
    return render(request, "index.html")

def link(request):
    return render(request, "link.html")

def portfolio(request):
    return render(request, "portfolio.html")

def contact(request):
    return render(request, "contact.html")

def experience(request):
    return render(request, "experience.html")

def gallery(request):
    return render(request, "gallery.html")
