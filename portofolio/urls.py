from django.contrib import admin
from django.urls import path
from .views import index, link, portfolio, contact, experience, gallery

urlpatterns = [
    path('', index),
    path('link', link),
    path('portfolio', portfolio),
    path('contact', contact),
    path('experience', experience),
    path('gallery', gallery),
]
